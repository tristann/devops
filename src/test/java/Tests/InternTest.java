package Tests;

import Models.Group;
import Models.Intern;
import Models.Internship;
import java.util.ArrayList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class InternTest {
    
    private Intern instance;
    
    public InternTest() {
    }
    
    @Before
    public void setUp() {
        instance=new Intern(1,"TestLinkedIn","TestLastName","TestFirstName");
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of IdProperty method, of class Intern.
     */
    @Test
    public void testIdProperty() {
        System.out.println("IdProperty");
        int id = 1;
        int Result=instance.getId();
        assertEquals(Result, id);
        
        instance.setId(2);
        id=2;
        Result=instance.getId();
        assertEquals(Result, id);
    }


    /**
     * Test of GroupProperty method, of class Intern.
     */
    @Test
    public void testGroupProperty() {
        System.out.println("GroupProperty");
        Group expResult = new Group();
        Group Result=instance.getGroup();
        assertEquals(expResult.toString(),Result.toString());
        
        
        Group newGroupe=new Group(1,"TestGroup");
        instance.setGroup(newGroupe);
        expResult=newGroupe;
        Result=instance.getGroup();
        assertEquals(expResult,Result);
    }


    /**
     * Test of LinkedinProperty method, of class Intern.
     */
    @Test
    public void testLinkedinProperty() {
        System.out.println("LinkedInProperty");
        String expResult = "TestLinkedIn";
        String Result=instance.getLinkedin();
        assertEquals(expResult,Result);
        
        instance.setLinkedin("LinkedIn");
        expResult="LinkedIn";
        Result=instance.getLinkedin();
        assertEquals(expResult,Result);
    }

    /**
     * Test of ListInternshipProperty method, of class Intern.
     */
    @Test
    public void testListInternshipProperty() {
        System.out.println("ListInternshipProperty");
        ArrayList<Internship> expResult = new ArrayList<Internship>();
        ArrayList<Internship> Result = instance.getInternships();
        assertEquals(expResult, Result);
        
        ArrayList<Internship> newListInternship=new ArrayList<Internship>();
        newListInternship.add(new Internship(1, "TestDescription"));
        instance.setListInternship(newListInternship);
        expResult=newListInternship;
        Result=instance.getInternships();
        assertEquals(expResult, Result);
        
    }

    /**
     * Test of toString method, of class Intern.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = String.format("(%s)-[Id : %d, Lastname : %s, Firstname : %s, Group : %s, Linkedin : %s, Internship nb : %d]", "intern".toUpperCase(),instance.getId(),instance.getLastname(),instance.getFirstname(),instance.getGroup().getName(),instance.getLinkedin(),instance.getInternships().size());
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
