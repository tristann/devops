package Tests;

import Models.Company;
import Models.Group;
import Models.Intern;
import Models.Internship;
import Models.Tutor;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static utils.Constants.*;

public class InternshipTest {
    
    private Internship instance;
    
    public InternshipTest() {
    }
    
    @Before
    public void setUp() {
        instance=new Internship(1, "TestMissionDescription");
    }

    /**
     * Test of IdProperty method, of class Internship.
     */
    @Test
    public void testIdProperty() {
        System.out.println("IdProperty");
        int id = 1;
        int Result=instance.getId();
        assertEquals(Result, id);
        
        instance.setId(2);
        id=2;
        Result=instance.getId();
        assertEquals(Result, id);
    }


    /**
     * Test of DateStartProperty method, of class Internship.
     */
    @Test
    public void testDateStartProperty() {
        System.out.println("DateStartProperty");
        String expResult = DEFAULT_VALUE;
        String Result=instance.getDateStart();
        assertEquals(expResult,Result);
        
        instance.setDateStart("20/02/2020");
        expResult="20/02/2020";
        Result=instance.getDateStart();
        assertEquals(expResult,Result);
    }


    /**
     * Test of DateEndProperty method, of class Internship.
     */
    @Test
    public void testDateEndProperty() {
        System.out.println("DateEndProperty");
        String expResult = DEFAULT_VALUE;
        String Result=instance.getDateEnd();
        assertEquals(expResult,Result);
        
        instance.setDateEnd("02/02/2022");
        expResult="02/02/2022";
        Result=instance.getDateEnd();
        assertEquals(expResult,Result);
    }

    /**
     * Test of TutorProperty method, of class Internship.
     */
    @Test
    public void testTutorProperty() {
        System.out.println("TutorProperty");
        Tutor expResult = new Tutor();
        Tutor Result=instance.getTutor();
        assertEquals(expResult.toString(),Result.toString());
        
        Tutor newTutor=new Tutor(1,"TestLogin","TestPassword","TestLastName","TestSurName");
        instance.setTutor(newTutor);
        expResult=newTutor;
        Result=instance.getTutor();
        assertEquals(expResult,Result);
    }


    /**
     * Test of CompanyProperty method, of class Internship.
     */
    @Test
    public void testCompanyProperty() {
        System.out.println("TutorProperty");
        Company expResult = new Company();
        Company Result=instance.getCompany();
        assertEquals(expResult.toString(),Result.toString());
        
        Company newCompany=new Company(1,"TestCompany","TectCompany_Adress");
        instance.setCompany(newCompany);
        expResult=newCompany;
        Result=instance.getCompany();
        assertEquals(expResult,Result);
    }


    /**
     * Test of InternProperty method, of class Internship.
     */
    @Test
    public void testInternProperty() {
        System.out.println("InternProperty");
        Intern expResult = new Intern();
        Intern Result=instance.getIntern();
        assertEquals(expResult.toString(),Result.toString());
        
        Intern newTutor=new Intern(1,new Group(1,"TestGroup"),"TestPassword","TestLastName","TestSurName");
        instance.setIntern(newTutor);
        expResult=newTutor;
        Result=instance.getIntern();
        assertEquals(expResult,Result);
    }


    /**
     * Test of MissionDescProperty method, of class Internship.
     */
    @Test
    public void testMissionDescProperty() {
        System.out.println("MissionDescProperty");
        String expResult = "TestMissionDescription";
        String Result=instance.getMissionDesc();
        assertEquals(expResult,Result);
        
        instance.setMissionDesc("Mission");
        expResult="Mission";
        Result=instance.getMissionDesc();
        assertEquals(expResult,Result);
    }


    /**
     * Test of TechGradeProperty method, of class Internship.
     */
    @Test
    public void testTechGradeProperty() {
        System.out.println("TechGradeProperty");
        float expResult = 0f;
        float Result=instance.getTechGrade();
        //a small delta in case of bad math rounding
        assertEquals(expResult,Result,0.0001f);
        
        instance.setTechGrade(20.0f);
        expResult=20.0f;
        Result=instance.getTechGrade();
        //a small delta in case of bad math rounding
        assertEquals(expResult,Result,0.0001f);
    }

    /**
     * Test of ComGradeProperty method, of class Internship.
     */
    @Test
    public void testComGradeProperty() {
        System.out.println("ComGradeProperty");
        float expResult = 0f;
        float Result=instance.getComGrade();
        //a small delta in case of bad math rounding
        assertEquals(expResult,Result,0.0001f);
        
        instance.setComGrade(20.0f);
        expResult=20.0f;
        Result=instance.getComGrade();
        //a small delta in case of bad math rounding
        assertEquals(expResult,Result,0.0001f);
    }


    /**
     * Test of isSpecification method, of class Internship.
     */
    @Test
    public void testIsSpecification() {
        System.out.println("isSpecification");
        boolean expResult = false;
        boolean result = instance.isSpecification();
        assertEquals(expResult, result);
        instance.setSpecification(true);
        expResult=true;
        result = instance.isSpecification();
        assertEquals(expResult, result);
    }


    /**
     * Test of isVisitSheet method, of class Internship.
     */
    @Test
    public void testIsVisitSheet() {
        System.out.println("isVisitSheet");
        boolean expResult = false;
        boolean result = instance.isVisitSheet();
        assertEquals(expResult, result);
        
        instance.setVisitSheet(true);
        expResult=true;
        result = instance.isVisitSheet();
        assertEquals(expResult, result);
    }


    /**
     * Test of isEvalEntSheet method, of class Internship.
     */
    @Test
    public void testIsEvalEntSheet() {
        System.out.println("isEvalEntSheet");
        boolean expResult = false;
        boolean result = instance.isEvalEntSheet();
        assertEquals(expResult, result);
        
        instance.setEvalEntSheet(true);
        expResult=true;
        result = instance.isEvalEntSheet();
        assertEquals(expResult, result);
    }


    /**
     * Test of isWebSurvey method, of class Internship.
     */
    @Test
    public void testIsWebSurvey() {
        System.out.println("isWebSurvey");
        boolean expResult = false;
        boolean result = instance.isWebSurvey();
        assertEquals(expResult, result);
        
        instance.setWebSurvey(true);
        expResult=true;
        result = instance.isWebSurvey();
        assertEquals(expResult, result);
    }


    /**
     * Test of isReport method, of class Internship.
     */
    @Test
    public void testIsReport() {
        System.out.println("isReport");
        boolean expResult = false;
        boolean result = instance.isReport();
        assertEquals(expResult, result);
       
        instance.setReport(true);
        expResult=true;
        result = instance.isReport();
        assertEquals(expResult, result);
    }


    /**
     * Test of isDefense method, of class Internship.
     */
    @Test
    public void testIsDefense() {
        System.out.println("isDefense");
        boolean expResult = false;
        boolean result = instance.isDefense();
        assertEquals(expResult, result);
        
        instance.setDefense(true);
        expResult=true;
        result = instance.isDefense();
        assertEquals(expResult, result);
    }


    /**
     * Test of isVisitPlanified method, of class Internship.
     */
    @Test
    public void testIsVisitPlanified() {
        System.out.println("isVisitPlanified");
        boolean expResult = false;
        boolean result = instance.isVisitPlanified();
        assertEquals(expResult, result);
        
        instance.setVisitPlanified(true);
        expResult=true;
        result = instance.isVisitPlanified();
        assertEquals(expResult, result);
    }


    /**
     * Test of isVisitDone method, of class Internship.
     */
    @Test
    public void testIsVisitDone() {
        System.out.println("isVisitDone");
        boolean expResult = false;
        boolean result = instance.isVisitDone();
        assertEquals(expResult, result);
        
        instance.setVisitDone(true);
        expResult=true;
        result = instance.isVisitDone();
        assertEquals(expResult, result);
    }


    /**
     * Test of CommentProperty method, of class Internship.
     */
    @Test
    public void testCommentProperty() {
        System.out.println("CommentProperty");
        String expResult = DEFAULT_VALUE;
        String Result=instance.getComment();
        assertEquals(expResult,Result);
        
        instance.setComment("TestComment");
        expResult="TestComment";
        Result=instance.getComment();
        assertEquals(expResult,Result);
    }


    /**
     * Test of toString method, of class Internship.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        
        String expResult = String.format("(%s)-[Id : %d, Start : %s, End : %s, Tutor : %s, Company : %s, Intern : %s, Description : %s]", "internship".toUpperCase(),instance.getId(),instance.getDateStart(),instance.getDateEnd(),instance.getTutor().getLastname()+","+instance.getTutor().getFirstname(),instance.getCompany().getName(),instance.getIntern().getLastname()+","+instance.getIntern().getFirstname(),instance.getMissionDesc());
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
