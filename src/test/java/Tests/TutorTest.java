package Tests;

import Models.Group;
import Models.Intern;
import Models.Tutor;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;


public class TutorTest {
    
    private Tutor instance;
    
    public TutorTest() {
    }
    
    @Before
    public void setUp() {
        instance=new Tutor(1,"TestLogin","TestPassword","TestLastName","TestFirstName");
    }

    /**
     * Test of IdProperty method, of class Tutor.
     */
    @Test
    public void testIdProperty() {
        System.out.println("IdProperty");
        int id = 1;
        int Result=instance.getId();
        assertEquals(Result, id);
        
        instance.setId(2);
        id=2;
        Result=instance.getId();
        assertEquals(Result, id);
    }

    /**
     * Test of LoginProperty method, of class Tutor.
     */
    @Test
    public void testLoginProperty() {
        System.out.println("LoginProperty");
        String expResult = "TestLogin";
        String Result=instance.getLogin();
        assertEquals(expResult,Result);
        
        instance.setLogin("Login");
        expResult="Login";
        Result=instance.getLogin();
        assertEquals(expResult,Result);
    }

    /**
     * Test of PasswordProperty method, of class Tutor.
     */
    @Test
    public void testPasswordProperty() {
        System.out.println("PasswordProperty");
        String expResult = "TestPassword";
        String Result=instance.getPassword();
        assertEquals(expResult,Result);
        
        instance.setPassword("Password");
        expResult="Password";
        Result=instance.getPassword();
        assertEquals(expResult,Result);
    }


    /**
     * Test of InternsProperty method, of class Tutor.
     */
    @Test
    public void testInternsProperty() {
        System.out.println("getInterns");
        Tutor instance = new Tutor();
        ArrayList<Intern> expResult = new ArrayList<>();
        ArrayList<Intern> result = instance.getInterns();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of ListeInternProperty method, of class Tutor.
     */
    @Test
    public void testListeInternProperty() {
        System.out.println("ListInternProperty");
        ArrayList<Intern> expResult = new ArrayList<Intern>();
        ArrayList<Intern> Result = instance.getInterns();
        assertEquals(expResult, Result);
        
        ArrayList<Intern> newListIntern=new ArrayList<Intern>();
        newListIntern.add(new Intern(1, new Group(1,"TestGroup"),"TestLinkedIn","TestLastName","TestSurName"));
        instance.setListeIntern(newListIntern);
        expResult=newListIntern;
        Result=instance.getInterns();
        assertEquals(expResult, Result);
    }

    /**
     * Test of toString method, of class Tutor.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = String.format("(%s)-[Id : %d, Lastname : %s, Firstname : %s, Login : %s, Interns nb : %d]", "tutor".toUpperCase(),instance.getId(),instance.getLastname(),instance.getFirstname(),instance.getLogin(),instance.getInterns().size());//"Tutor{" + "id=" + instance.getId() + ", login=" + instance.getLogin() + ", password=" + instance.getPassword()+ "}";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
